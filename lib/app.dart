import 'package:flutter/material.dart';
import 'dart:math';

class App extends StatefulWidget {
  final posts;
  App( this.posts, {Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new AppState();
  }
}

class AppState extends State<App> {

  //LIST VIEW WIDGET FOR POSTS
  Widget renderPosts(posts, theme) {
    final widgets = <Widget>[];
    for(int i = 0; i < posts.length - 90; i++){
        final List<String> images = <String>["land1.jpg", "land2.jpg", "land3.jpg"];
        final Random rng = new Random();
        final String image = images[rng.nextInt(images.length)];

        widgets.add(
          new Container(
            padding: const EdgeInsets.all(10.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                  new Container(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: new Text(
                      "${posts[i]["title"]}",
                      textDirection: TextDirection.ltr,
                      style: new TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),

                  new Container(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: new Image(
                      image: new AssetImage('images/'+image), 
                    )
                  ),

                  new Container(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: new Text(
                      "${posts[i]["body"]}",
                      textDirection: TextDirection.ltr,
                      style: new TextStyle(
                        fontSize: 12.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),

                  new Container(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new IconButton(
                          icon: new Icon(Icons.favorite, color: theme.primaryColor),
                          onPressed: () => debugPrint("Image $image added to favorite."),
                        ),
                        new IconButton(
                          icon: new Icon(Icons.share, color: theme.primaryColor),
                          onPressed: () => debugPrint("Image $image shared with your friends."),
                        ),
                      ],
                    )
                  ),

                  new Divider(height: 4.9, color: theme.primaryColor)
              ],
            ) 
        )
      );    
    }
    return new ListView(
      children: widgets
    );
  }


  //APP BAR WIDGET
  Widget renderAppBar() {
    return new AppBar(
      title: new Text(
        "Landscapes",
        style: new TextStyle(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
          color: Colors.white
        )
      )
    );
  }


  //BOTTOM NAVIGATION BAR WIDGET
  Widget renderBottomNavigationBar(theme) {
    return new BottomNavigationBar(
      currentIndex: 1,
      fixedColor: Theme.of(context).accentColor,
      items: <BottomNavigationBarItem>[
        new BottomNavigationBarItem(
          icon: new Icon(Icons.person, color: theme.primaryColor),
          title: new Text("Profile", style: new TextStyle(color: theme.primaryColor)),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(Icons.image, color: theme.primaryColor),
          title: new Text("Landscapes", style: new TextStyle(color: theme.primaryColor)),
        ),
        new BottomNavigationBarItem(
          icon: new Icon(Icons.message, color: theme.primaryColor),
          title: new Text("Messages", style: new TextStyle(color: theme.primaryColor))
        ),
        new BottomNavigationBarItem(
          icon: new Icon(Icons.settings, color: theme.primaryColor),
          title: new Text("Settings", style: new TextStyle(color: theme.primaryColor))
        )
      ],
      onTap: (int i) => debugPrint("tapped $i icon")
    );
  }
 
  @override
  Widget build(BuildContext context) {

    var theme = new ThemeData(
      primaryColor: Colors.green,
      accentColor: Colors.greenAccent,
      canvasColor: Colors.white
    );

    return new MaterialApp(
      theme: theme,
      home: new Scaffold(
        appBar: renderAppBar(),
        body: renderPosts(widget.posts, theme),
        bottomNavigationBar: renderBottomNavigationBar(theme)
      )
    );
    
  }

}