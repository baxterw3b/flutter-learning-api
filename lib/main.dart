import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import './app.dart';

void main() async {
  final posts = await getData();
  runApp(new App(posts));
}

Future<List> getData() async {
  final String api = "https://jsonplaceholder.typicode.com/posts";
  final http.Response response = await http.get(api);
  return json.decode(response.body);
}

